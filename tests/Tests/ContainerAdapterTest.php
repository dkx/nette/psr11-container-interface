<?php

declare(strict_types=1);

namespace NettePsr11Tests\Tests;

use DKX\NettePsr11\ContainerAdapter;
use Nette\DI\Compiler;
use Nette\DI\ContainerLoader;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

final class ContainerAdapterTest extends TestCase
{
	public function testGet_notFound(): void
	{
		$this->expectException(NotFoundExceptionInterface::class);

		$container = $this->createContainer();
		$container->get('unknown');
	}

	public function testGet(): void
	{
		$container = $this->createContainer(function (Compiler $compiler) {
			$compiler->getContainerBuilder()
				->addDefinition('service')
				->setType(TestService::class);
		});

		self::assertInstanceOf(TestService::class, $container->get(TestService::class));
	}

	public function testHas_false(): void
	{
		$container = $this->createContainer();
		self::assertFalse($container->has('unknown'));
	}

	public function testHas_true(): void
	{
		$container = $this->createContainer(function (Compiler $compiler) {
			$compiler->getContainerBuilder()
				->addDefinition('service')
				->setType(TestService::class);
		});

		self::assertTrue($container->has(TestService::class));
	}

	private function createContainer(?callable $generator = null): ContainerInterface
	{
		$loader = new ContainerLoader(__DIR__. '/../temp',true);
		$class = $loader->load($generator ?? function (Compiler $compiler) {}, $this->getName());
		return new ContainerAdapter(new $class());
	}
}

class TestService {}
