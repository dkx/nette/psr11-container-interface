# DKX/Nette/PSR11 ContainerInterface

Nette DI Container adapter for PSR11

## Installation

```bash
$ composer require dkx/nette-psr11-container-interface
```

## Usage

```php
<?php

use DKX\NettePsr11\ContainerAdapter;
use Nette\DI\Container;

$netteContainer = new Container;
$container = new ContainerAdapter($netteContainer);
```
