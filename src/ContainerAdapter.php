<?php

declare(strict_types=1);

namespace DKX\NettePsr11;

use DKX\NettePsr11\Exception\ContainerException;
use DKX\NettePsr11\Exception\NotFoundException;
use Nette\DI\Container;
use Nette\DI\MissingServiceException;
use Psr\Container\ContainerInterface;

final class ContainerAdapter implements ContainerInterface
{
	/** @var \Nette\DI\Container */
	private $container;

	public function __construct(Container $container)
	{
		$this->container = $container;
	}

	public function get($id)
	{
		try {
			return $this->container->getByType($id);
		} catch (MissingServiceException $e) {
			throw new NotFoundException($e->getMessage(), $e->getCode(), $e);
		} catch (\Exception $e) {
			throw new ContainerException($e->getMessage(), $e->getCode(), $e);
		}
	}

	public function has($id)
	{
		return $this->container->getByType($id, false) !== null;
	}
}
