<?php

declare(strict_types=1);

namespace DKX\NettePsr11\Exception;

use Psr\Container\ContainerExceptionInterface;

final class ContainerException extends \Exception implements ContainerExceptionInterface
{
}
