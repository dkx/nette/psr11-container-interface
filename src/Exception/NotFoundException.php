<?php

declare(strict_types=1);

namespace DKX\NettePsr11\Exception;

use Psr\Container\NotFoundExceptionInterface;

final class NotFoundException extends \InvalidArgumentException implements NotFoundExceptionInterface
{
}
